#! /bin/tcsh

# Put here the actual path of the base of the TeX distribution tree
set teTeX=/usr/share/texmf
if ( ! -e $teTeX ) then
  echo "The teTeX variable may be wrong or teTeX may not be instaled"
  exit(1)
endif

echo " "
echo Searching for files with wrong names
set all=`find $teTeX -name "*[Pp]ortuges*" -print`
foreach f ($all)
  set g=`echo $f | sed -e s/portuges/portugues/g -e s/Portuges/Portugues/g`
  echo "  renaming" $f
  echo "        to" $g
  /bin/mv $f $g
end


echo " "
echo Searching for files with wrong contents
set all=`find $teTeX -type f -exec grep -l "[Pp]ortuges" {} \;`
set g=/tmp/mod.$$
/bin/rm -f $g
foreach f ($all)
  if ( "$f" =~ *.dvi ) then
    echo "    skiping" $f "(dvi)"
  else
    set h=`file $f`
    if ( "$h" =~ *ELF* ) then
      echo "    skiping" $f "(executable)"
    else
      echo "  modifying" $f
      sed -e s/portuges/portugues/g -e s/Portuges/Portugues/g <$f >$g
      /bin/mv -f $g $f
      chmod a+r $f
      chmod go-w $f
    endif
  endif
end


echo " "
echo Done
