#ifndef __ROBFUNC_H__
#define __ROBFUNC_H__

#include "RobSock.h"

#define RUN         1
#define STOP        2
#define WAIT        3
#define RETURN      4
#define FINISHED    5

#define NORTH	    0x01
#define SOUTH       0x02
#define EAST	    0x04
#define WEST        0x08


#define PI         3.14
#define N 			PI/2
#define S 			-PI/2
#define E 			0
#define W 			PI
struct mapa{
	int pos;
	int wall;
	double orientacao;
	int decissions;
	int reach;
	int x;
	int y;
	int cost;
};

float currentx,currenty,nextx,nexty;

typedef union
{
	struct 
	{
		double obstSensRight; ///< right obstacle sensor
		double obstSensLeft;
		double obstSensFront; ///< frontal obstacle sensor
	};
	double array[6];
} MR32_analogSensors;

extern MR32_analogSensors analogSensors;
extern int currentpos,antpos;
extern struct mapa MAPA[28][28];
extern double xPos;
extern double yPos;
extern double teta;
extern int chao;
void DetermineAction(int beaconToFollow, float *lPow, float *rPow);
void driveWall(float *lPow, float *rPow);		//PD para seguir so a parede da Direita
double rotate(float *lPow, float *rPow,double desirePos,int dir);
void calTeta(float *lPow,float *rPow);			//calcula apenas a orientacao actual
void calPos(float *lPow,float *rPow);			//calcula a possicao actual e a sua orientacao
void resetPos(void);							//fazer reset a posicao
void initMap(void);								//inicializacao do mapa
void mapAct(int *yA,int *xA);//void);								//actualiza o mapa actual
void printMap(void);							//imprime o mapa no ecra

void moveForward(float *lPow,float *rPow);		//andar em frente as chegas
void driveWallLeft(float *lPow,float *rPow);	//PD para seguir so a parede da esquerda
void driveWallRight(float *lPow,float *rPow);	//PD para estar centrado com duas paredes

float orierror(void);
void cleanSensors(void); // limpa a memoria dos sensores
int findCloser(int *yA,int *xA);
int rotateRel(float *lPow,float *rPow,float maxVel,float *deltaAngle);	

void actualizaXY(int *yA,int *xA);

int compara(int *k);

#endif
