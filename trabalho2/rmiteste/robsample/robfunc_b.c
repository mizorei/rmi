#include "robfunc.h"
#include <stdio.h>
#include <math.h>
#define KP 0.2
#define KD 0.005

#define MEDIAN_SIZE	   4             // Median buffer size for obstacle sensor filtering              
#define AVERAGE_SIZE	   3              // Average buffer size for obstacle sensor filtering              


extern double error,u,error_ant,errorA;
extern double lerror,lu,lerror_ant;
void driveWall(float *lPow, float *rPow);
void calPos(float *lPow,float *rPow);

double Right[7]= {0,0,0,0,0,0,0};
double Center[7]={0,0,0,0,0,0,0};
double Left[7]=  {0,0,0,0,0,0,0};
int Arraysize=7;

double xPos=0;
double yPos=0;
double teta=0;

int rotate(float *lPow, float *rPow,double desireRotation);


typedef union
{
	struct 
	{
		double obstSensRight; ///< right obstacle sensor
		double obstSensLeft;
		double obstSensFront; ///< frontal obstacle sensor
	};
	double array[6];
} MR32_analogSensors;



MR32_analogSensors analogSensors;

double average(double array[7],double val){
	double aux=0;
	int i;
	for (i=Arraysize-1;i>0;i--){	
		array[i]=array[i-1];
		aux+=array[i];
	}
	array[0]=val;

	aux += val;
	aux=aux/Arraysize;
	return aux;

}


/* Calculate the power of left and right motors */
void DetermineAction(int beaconToFollow, float *lPow, float *rPow)
{
	static int counter=0;
	int flag=0;
	bool   beaconReady;
	static struct beaconMeasure beacon;
	static float  left, right, center;
	static int    Ground;
	static bool   Collision;


	double filteredValue =0;

	//printf("verifico obstaculos\n");
	if(IsObstacleReady(LEFT)){
		left=     GetObstacleSensor(LEFT);
		filteredValue = average(Left,left);
		analogSensors.array[1] = filteredValue;
		//	printf("left %f \n",left);
	}
	if(IsObstacleReady(RIGHT)){
		right=    GetObstacleSensor(RIGHT);
		filteredValue = average(Right,right);
		analogSensors.array[0] = filteredValue;
		//	printf("right %f \n",right);
	}
	if(IsObstacleReady(CENTER)){
		center=   GetObstacleSensor(CENTER);
		filteredValue = average(Center,center);
		analogSensors.array[2] = filteredValue;
		//	printf("center %f \n",center);
	}
	//printf("obstaculos ok \n");
	//printf("verificar beacon ....");
	beaconReady = IsBeaconReady(beaconToFollow);
	if(beaconReady) {
		beacon =  GetBeaconSensor(beaconToFollow);
	}
	else beaconReady=0;
	//printf("beacon ok \n");
	//printf("verificar ground e bumper\n");
	if(IsGroundReady())
		Ground=    GetGroundSensor();
	if(IsBumperReady())
		Collision= GetBumperSensor();
	//printf("ok \n");
	//printf("close obstacle \n");	
	if(center>4.5 || right>4.5 || left>4.5 || Collision) { /* Close Obstacle - Rotate */
		if(counter % 400 < 200) {
			*lPow=0.06;
			*rPow=-0.06; }
		else {
			*lPow=-0.06;
			*rPow=0.06; }
	}
	else if(right>1.5) { /* Obstacle Near - Avoid */
		*lPow=0.0;
		*rPow=0.05;
	}
	else if(left>1.5) {
		*lPow=0.05;
		*rPow=0.0;
	}
	else { 
		if(beaconReady && beacon.beaconVisible && beacon.beaconDir>20.0) { /* turn to Beacon */
			*lPow=0.0;
			*rPow=0.1;
		}
		else if(beaconReady && beacon.beaconVisible && beacon.beaconDir<-20.0) {
			*lPow=0.1;
			*rPow=0.0;
		}
		else { /* Full Speed Ahead */
			*lPow=0.1;
			*rPow=0.1;
		}
	}
	//printf("ok \n");
	/*if(beaconReady && beacon.beaconVisible && beacon.beaconDir>20.0) {  turn to Beacon */
	/*	*lPow=0.0;
	 *rPow=0.1;
	 }
	 else if(beaconReady && beacon.beaconVisible && beacon.beaconDir<-20.0) {
	 *lPow=0.1;
	 *rPow=0.0;
	 }
	 else {  Full Speed Ahead */
	/*	*lPow=0.1;
	 *rPow=0.1;
	 }*/
	//printf("estou aqui \n");
	driveWall(lPow,rPow);
	//printf("ola");
	counter++;
	if((xPos>=6) && (flag==0)){
		while(flag==0){

			flag=rotate(lPow,rPow,180);
		}
		flag=1;
	}
	if(flag==1 && xPos <0){
		*lPow=0;
		*rPow=0;
	}
}





//implementacao do pid e fazer com que o robo se centre com as paredes a medida que anda em frente
void driveWall(float *lPow, float *rPow){
	//printf("ler analog\n");
	//printf("foi lido\n");
	error_ant=error;
	//error= 0 - ;//ver o que se vai considerar o erro tendo em conta os censores
	error= -2+analogSensors.array[0];
	u= 0.01*error+0.002*(error - error_ant);
	lerror_ant=lerror;
	lerror = -2+analogSensors.array[1]; 
	lu= 0.01*lerror+0.002*(lerror - lerror_ant);
	if(lu>0.12)
		lu=0.12;
	if (lu<-0.12)
		lu=-0.12;
	if(u>0.12)
		u=0.12;
	if(u<-0.12)
		u=-0.12;
	*lPow=0.03+lu;
	*rPow= 0.03+u;
	calPos(lPow,rPow);
	//printf("lpow %f \n,rPow %f \n, lu %f \n, u%f \n",*lPow,*rPow, lu,u);
	//printf("\n sensRight %f\n, sensLeft %f\n center %f\n", analogSensors.array[0],analogSensors.array[1],analogSensors.array[2]);
	double aux = teta*180/3.141592;
	printf("\n xpos-> %f \n yPos-> %f \n aux-> %f \n teta-> %f \n ",xPos,yPos,aux,teta);
}

//rodar 360º e fazer o mapeamento quando o robo inicialisa no mapa
/*void rotateMap(float *lPow, float *rPow){

}*/

//dado ao conhecimento do mapa as rotacoes irao ser adaptadas para multiplos de 90º
int rotate(float *lPow, float *rPow,double desireRotation){
	int flag=1;
	*lPow=0.02;
	*rPow=-0.02;
	double aux=0;
	double angulo = fabs(teta)*180/3.141592+desireRotation;
	//angulo=angulo*180/3.141592;
	if(angulo >=360)
		angulo=angulo-360;
	if(angulo <=-360)
		angulo=angulo+360;
	do{
		DriveMotors(*lPow,*rPow);
		calTeta(lPow,rPow);
		aux=fabs(teta)*180/3.141592;
		if((aux<(angulo+0.6)) || (aux>(angulo-0.6)))
			flag=0;
	}while(flag==1);


	//dado que se parou e se rodou as variaveis dao reset
	error=0;
	errorA=0;
	error_ant=0;
	lerror_ant=0;
	lerror = 0;
	return 1;	
}


//calcular a posicao actual do robo
void calTeta(float *lPow,float *rPow){
	teta+= (*rPow-*lPow);
	//if(teta>=360)
//		teta=teta-360;
//	if(teta<=-360)
//		teta=teta+360;
}
void calPos(float *lPow,float *rPow){
	teta+= (*rPow-*lPow);
	double Lin=(*lPow+*rPow)/2;
	xPos=xPos+cos(teta)*Lin;
	yPos=yPos+sin(teta)*Lin;


}


void resetPos(void){
	xPos=0;
	yPos=0;
	teta=0;
}

