#include "robfunc.h"
#include <stdio.h>
#include <math.h>
#define KP 0.2
#define KD 0.005

#define MEDIAN_SIZE	   4             // Median buffer size for obstacle sensor filtering              
#define AVERAGE_SIZE	   3              // Average buffer size for obstacle sensor filtering              

#define STOPED		0
#define DRIVEFOR	1
#define ROTATELEFT	2
#define ROTATERIGHT	3

#define FINDCHEESE	1
#define RETURNING	2

#define PBASE		0.05
struct mapa MAPA[28][28];
int currentpos=0,antpos=0;
int chao;
extern double error,u,error_ant,errorA;
extern double lerror,lu,lerror_ant;
//extern int xA,yA;
/*
   double Right[7]= {0,0,0,0,0,0,0};
   double Center[7]={0,0,0,0,0,0,0};
   double Left[7]=  {0,0,0,0,0,0,0};
 */
extern double Right[7] ,Center[7] ,Left[7];
int Arraysize=7;

double xPos=0;
double yPos=0;
double teta=0;
int imprimeMapa=1;		//imprimir o mapa
extern int map[28][28];


int STATE=FINDCHEESE;
int ACTION=DRIVEFOR;
int flag=1;
int andou=0;

double normalizeAngle(double angle)
{
   while( angle <= -PI ) angle += 2.0 * PI;
   while( angle > PI ) angle -= 2.0 * PI;
   return angle;
}

MR32_analogSensors analogSensors;



double average(double array[7],double val){
	double aux=0;
	int i;
	for (i=Arraysize-1;i>0;i--){	
		array[i]=array[i-1];
		aux+=array[i];
	}
	array[0]=val;

	aux += val;
	aux=aux/Arraysize;
	return aux;

}
void cleanSensors(void){
/*	int i;
	for (i=0;i<Arraysize;i++){	
		Left[i]=0;
		Center[i]=0;
		Right[i]=0;
	}
	*/error_ant=orierror();
	error=orierror();
	u=0;
}

float orierror(void){
	if ( fabs(teta )<0.79){
		return E;
	}else if ( teta> 0.9 && teta<2){ //orientacao = 90	E W S N
		return N;
	}else if (teta> -1.8 && teta<-0.9){ //orientacao = 270  W E N S
		return S;
	}else{//if (teta<-2.7 || teta >2.7){ //orientacao = 180  S N W E
		return W;//W;
	}
	return 0;
}

/* Calculate the power of left and right motors */
void DetermineAction(int beaconToFollow, float *lPow, float *rPow)
{
	static int counter=0;
	bool   beaconReady;
	static struct beaconMeasure beacon;
	static float  left, right, center;
	static int    Ground;
	static bool   Collision;


	double filteredValue =0;

	//printf("verifico obstaculos\n");
	if(IsObstacleReady(LEFT)){
		left=     GetObstacleSensor(LEFT);
		filteredValue = average(Left,left);
		analogSensors.array[1] = filteredValue;
		//	printf("left %f \n",left);
	}
	if(IsObstacleReady(RIGHT)){
		right=    GetObstacleSensor(RIGHT);
		filteredValue = average(Right,right);
		analogSensors.array[0] = filteredValue;
		//	printf("right %f \n",right);
	}
	if(IsObstacleReady(CENTER)){
		center=   GetObstacleSensor(CENTER);
		filteredValue = average(Center,center);
		analogSensors.array[2] = center;
		//	printf("center %f \n",center);
	}
	if(IsCompassReady()){
		teta= (GetCompassSensor()*PI/180);
	}
	//printf("obstaculos ok \n");
	//printf("verificar beacon ....");
	beaconReady = IsBeaconReady(beaconToFollow);
	if(beaconReady) {
		beacon =  GetBeaconSensor(beaconToFollow);
	}
	else beaconReady=0;
	//printf("beacon ok \n");
	//printf("verificar ground e bumper\n");
	if(IsGroundReady())
		Ground=    GetGroundSensor();

	if(IsBumperReady())
		Collision= GetBumperSensor();

	counter++;
	double aux = teta*180/PI;
	chao=Ground;
		//printf(" left %f \n" , analogSensors.array[1]);
		//printf(" left %f \n" , analogSensors.array[0]);
		//printf(" left %f \n" , analogSensors.array[2]);
	printMap();
	printf("\n xpos-> %f \n yPos-> %f \n aux-> %f \n teta-> %f \n time %u \n",xPos,yPos,aux,teta, GetTime());
	//	printf("\n Center ->");
}
//implementacao do pid e fazer com que o robo se centre com as paredes a medida que anda em frente
void driveWall(float *lPow, float *rPow){
	//printf("ler analog\n");
	//printf("foi lido\n");
	float control;
	error_ant=error;
	float a=orierror();
	//error= 0 - ;//ver o que se vai considerar o erro tendo em conta os censores
	error= (2-analogSensors.array[0]);
	u= 0.02*error+0.005*(error - error_ant);
	lerror_ant=lerror;
	lerror = (2-analogSensors.array[1]); 
	lu= 0.02*lerror+0.005*(lerror - lerror_ant);
	control = 0.05+lu;
	if(control>0.15)
		control=0.15;
	if (control<-0.15)
		control=-0.15;
	*lPow=control;
	control = 0.05+u;
	if(control>0.15)
		control=0.15;
	if (control<-0.15)
		control=-0.15;
	*rPow= control;
	calPos(lPow,rPow);
	//printf("lpow %f \n,rPow %f \n, lu %f \n, u%f \n",*lPow,*rPow, lu,u);
	//printf("\n sensRight %f\n, sensLeft %f\n center %f\n", analogSensors.array[0],analogSensors.array[1],analogSensors.array[2]);
}
void moveForward(float *lPow,float *rPow){
	float control;
	error_ant=error;
	float err2=0;
	float a=orierror();
	float orienta=a;
	float err3=0;
//-----------------------------------------
	float c = round(yPos)-yPos;
	
	float b = round(xPos)-xPos;
	if( (orienta <N+0.2 && orienta >N-0.2 ) ){
		err3=  xPos > 0 ? b : -b;
		//err3=-0+b;		//erro em y
	}
	else if((orienta <S+0.2 && orienta >S-0.2)){
		err3 =  xPos < 0 ? b : -b;
		//err3=-b;//erro em X
	}
	else if ((orienta <E+0.2 && orienta >E-0.2))
	{
		err3 =  yPos > 0 ? c : -c;
		//err3=-c;
	}else
		err3 =  yPos < 0 ? c : -c;
		//err3=c;
//--------------------------------------------

	err2=0;
	if(a < -2.5 || a>2.5){
		error= normalizeAngle(-a+teta) + 0.5*err3;
		u= 0.45*error+0.014*(error - error_ant);
	}
	else{
		error= (-a+teta)+0.5*err3;//-a+teta;//analogSensors.array[2];11
		u= 0.45*error+0.014*(error - error_ant);
	}
	//u= 0.45*error+0.014*(error - error_ant);
	printf ("sinal de controlo %f \n",u);
	control = PBASE-u;
	if (control >0.15)
		control = 0.15;
	*rPow=control;
	control = PBASE+u;
	if (control < -0.15)
		control = -0.15;
	*lPow= control;
	calPos(lPow,rPow);
}
void driveWallLeft(float *lPow,float *rPow){
	float control;
	error_ant=error;
	float a=orierror();
	//error= 0 - ;//ver o que se vai considerar o erro tendo em conta os censores
	error= 2-analogSensors.array[1];//-a+teta;//analogSensors.array[1];
	u= 0.015*error+0.002*(error - error_ant);
	lerror_ant=lerror;
	control = PBASE+u;
	if (control >0.15)
		control = 0.15;
	*rPow=control;
	control = PBASE-u;
	if (control < -0.15)
		control = -0.15;
	*lPow= control;
	calPos(lPow,rPow);
}
void driveWallRight(float *lPow,float *rPow){
	float control;
	error_ant=error;
	float a=orierror();
	//error= 0 - ;//ver o que se vai considerar o erro tendo em conta os censores
	//error= -a+teta;//analogSensors.array[0];
	error= 2-analogSensors.array[0];
	u= 0.015*error+0.002*(error - error_ant);
	lerror_ant=lerror;
	control=PBASE-u;
	if (control >0.15)
		control = 0.15;
	*lPow=control;
	control = PBASE+u;
	if (control < -0.15)
		control = -0.15;
	*rPow= control;
	calPos(lPow,rPow);
}

//rodar 360º e fazer o mapeamento quando o robo inicialisa no mapa
/*void rotateMap(float *lPow, float *rPow){

  }*/

//dado ao conhecimento do mapa as rotacoes irao ser adaptadas para multiplos de 90º
double rotate(float *lPow, float *rPow,double desirePos,int dir){
	if(dir==1){
		*lPow=0.01;
		*rPow=-0.01;
		desirePos=-fabs(desirePos);
	}else{
		*lPow=-0.01;
		*rPow=0.01;
	}
	double aux;
	//double angulo = fabs(teta)*180/3.141592+desireRotation;
	//angulo=angulo*180/3.141592;
	calTeta(lPow,rPow);
	aux=fabs(teta);
	error=0;
	errorA=0;
	error_ant=0;
	lerror_ant=0;
	lerror = 0;
	return ( desirePos-aux);	
}


int rotateRel(float *lPow, float *rPow,float maxVel, float *deltaAngle){
	fprintf(stderr,"rotateRel lp %f rp %f mvel %f delta %f\n", *lPow, *rPow, maxVel ,*deltaAngle);
	double targetAngle;
	float errorRel;
	int errorSignOri;
	double cmdVel;
	calTeta(lPow,rPow);
	targetAngle = normalizeAngle(/*teta + */*deltaAngle);
	errorRel = normalizeAngle(targetAngle - teta);
	errorSignOri = errorRel < 0 ? -1 : 1;

	cmdVel = errorRel < 0 ? -maxVel : maxVel;
	*lPow=-cmdVel;
	*rPow= cmdVel;
	errorRel = normalizeAngle(targetAngle - teta);
		//printf(" errorRel %f\n ", errorRel);
	if (fabs(errorRel) > 0.01 && errorSignOri * errorRel > 0){
		return 0;
	}
	return 1;
}


//calcular a posicao actual do robo
void calTeta(float *lPow,float *rPow){
	//teta+= (*rPow-*lPow);
	//teta=normalizeAngle(teta);
}
void calPos(float *lPow,float *rPow){
	//teta= normalizeAngle(teta+(*rPow-*lPow));
	double Lin=(*lPow+*rPow)/2;
	xPos=xPos+cos(teta)*Lin;
	yPos=yPos+sin(teta)*Lin; 
}
void setPos(float x,float y){
	xPos=x;
	yPos=y;
}
void resetPos(void){
	xPos=0;
	yPos=0;
	teta=0;
}


void initMap(void){
	int nrows=28;
	int ncols=28;
	int i,j;
	for(i = 0; i < nrows; ++i)
	{
		for(j = 0; j < ncols ; ++j) 
		{	if(i==14 && j==14){
				MAPA[i][j].pos=0;
				MAPA[i][j].reach=0;
				//MAPA[i][j].wall=0x0B;
			}else{
				MAPA[i][j].pos=254;
				MAPA[i][j].reach=-1;
			}
			printf(" %d", MAPA[i][j].pos);
			
		}
		printf("\n");
	}
}

/*void checkWall(void){		//compara a parede existente com as celulas adjacentes

}*/


int placeWall(void){		//place walls acording to its orientation
	int aux2=0;
	printf ("place wall \n teta corrente %f \n analogSensors %f \n", teta, analogSensors.array[2]);
	//orientacao = 0	N S E W
	if ( fabs(teta )<0.8){
		if(analogSensors.array[1]>1.5)
			aux2= NORTH | aux2;
		if(analogSensors.array[0]>1.5)
			aux2 = aux2 | SOUTH;
		if(analogSensors.array[2]>0.7){
			aux2 = aux2 | EAST;
		}
	}else if ( teta> 0.8 && teta<1.68){ //orientacao = 90	E W S N
		if(analogSensors.array[1]>1.5)
			aux2= WEST | aux2;
		if(analogSensors.array[0]>1.5)
			aux2 = aux2 | EAST;
		if(analogSensors.array[2]>0.7){
			aux2 = aux2 | NORTH;
		}
	}else if (fabs(teta)<1.68){ //orientacao = 270  W E N S
		if(analogSensors.array[1]>1.5)
			aux2=  EAST | aux2;
		if(analogSensors.array[0]>1.5)
			aux2 = aux2 | WEST;
		if(analogSensors.array[2]>0.7){
			aux2 = aux2 | SOUTH;
		}
	}else if (fabs(teta)<3.2){ //orientacao = 180  S N W E
		if(analogSensors.array[1]>1.5)
			aux2= SOUTH | aux2;
		if(analogSensors.array[0]>1.5)
			aux2 = aux2 | NORTH;
		if(analogSensors.array[2]>0.7){
			aux2 = aux2 | WEST;
		}
	}
	return aux2;
}

void actualizaXY(int *yA,int *xA){
	int x,y;
	int aux;
	float a = fabs(round(yPos))-fabs(yPos);
	
	float b = fabs(round(xPos))-fabs(xPos);
	if (round(yPos)<yPos)
		y=(int) (yPos-a);
	else
		y=(int) (yPos+a);

	if (round(xPos)<xPos)
		x=(int) (xPos-b);
	else
		x=(int) (xPos+b);
	float orienta=orierror();
	if( (orienta <N+0.2 && orienta >N-0.2 ) || (orienta <S+0.2 && orienta >S-0.2) ){
		if ((int)fabs(fmod(yPos,2))==0 && fabs(a) <0.305){
			y=(int)(-y/2+14),x=(int)(x/2+14);
			aux=MAPA[y][x].pos;
			if(aux<254){
				*yA=y;
				*xA=x;
			//	cleanSensors();
			}
		}
	}else{
		if ((int)fabs(fmod(xPos,2))==0 && fabs(b) <0.305){
			y=(int)(-y/2+14),x=(int)(x/2+14);
			aux=MAPA[y][x].pos;
			if(aux<254){
				*yA=y;
				*xA=x;
			//	cleanSensors();
			}
		}
	}
}


void mapAct(int *yA,int *xA){//void){
	int x,y;
	int xaux=*xA,yaux=*yA;
	int aux;
	int cost;
	float a = fabs(round(yPos))-fabs(yPos);
	float b = fabs(round(xPos))-fabs(xPos);
	
	if (round(yPos)<yPos)
		y=(int) (yPos-a);
	else
		y=(int) (yPos+a);

	if (round(xPos)<xPos)
		x=(int) (xPos-b);
	else
		x=(int) (xPos+b);
	float orienta=orierror();
	if( (orienta <N+0.2 && orienta >N-0.2 ) || (orienta <S+0.2 && orienta >S-0.2) ){
		if ((int)fmod(round(yPos),2)==0 && fabs(a) <0.105){
			y=(int)(-y/2+14),x=(int)(x/2+14);
			aux=MAPA[y][xaux].pos;
			if(aux==254){

				cost = MAPA[y-1][xaux].pos < MAPA[y+1][xaux].pos ? MAPA[y-1][xaux].pos : MAPA[y+1][xaux].pos;
				MAPA[y][xaux].pos= ++cost;
				MAPA[y][xaux].orientacao= teta;
				MAPA[y][xaux].wall=placeWall();
				antpos=currentpos;
				currentpos=andou;
				*yA=y;
				//*xA=x;
				cleanSensors();
			}
		}
	}else{
		if ((int)fmod(xPos,2)==0 && fabs(b) <0.105){
			y=(int)(-y/2+14),x=(int)(x/2+14);
			aux=MAPA[yaux][x].pos;
			if(aux==254){
				cost = MAPA[yaux][x-1].pos < MAPA[yaux][x+1].pos ? MAPA[yaux][x-1].pos : MAPA[yaux][x+1].pos;
				MAPA[yaux][xaux].pos= ++cost;
				//MAPA[yaux][x].pos= ++andou;
				MAPA[yaux][x].orientacao= teta;
		
				MAPA[yaux][x].wall=placeWall();
				antpos=currentpos;
				currentpos=andou;
				//*yA=y;
				*xA=x;
				cleanSensors();
			}
		}
	}
	
		//cleanSensors();
		//setPos(round(xPos),round(yPos));
}

void printWal(void){
	int nrows=28;
	int ncols=28;
	int i,j;
	for(i = 0; i < nrows; ++i)
	{
		for(j = 0; j < ncols ; ++j) 
		{	
			printf(" %x", MAPA[i][j].wall);
		}
		printf("\n");
	}
	printf("\n");
}
void printMap(void){
	int nrows=28;
	int ncols=28;
	int i,j;
	if(imprimeMapa==1){
	for(i = 0; i < nrows; ++i)
	{
		for(j = 0; j < ncols ; ++j) 
		{	
			printf(" %03d", MAPA[i][j].pos);
		}
		printf("\n");
	}
	//printf("\n");
	printWal();
	}
}


int findCloser(int *yA,int *xA){
	int aux;
	int pos=MAPA[*yA][*xA].pos;
	aux=MAPA[*yA][*xA].wall;
	int y=*yA,x=*xA;
	//ver celulas adjacentes
	int i;
	
	for(i=pos-1;i>0;i--){
		if ( ( ((aux & NORTH)!=NORTH ) && (MAPA[y-1][x].pos==i)) ){
	
			return NORTH;
		}
		else if (  ( ((aux & SOUTH)!=SOUTH) && (MAPA[y+1][x].pos==i))  ){
	
			return SOUTH;
		}
		else if (  (((aux & EAST)!=EAST) && (MAPA[y][x+1].pos==i ))  ){
	
			return EAST;
		}
		else if ( (((aux & WEST)!=WEST) && (MAPA[y][x-1].pos==i ))   ){
	
			return WEST;
		}
	}
	return 0;
}

int compara (int *k){
	float orientacao =orierror();
	int aux = *k;
	printf("diufhvzdixocj %u\n",aux);
		if ((orientacao <N+0.2 && orientacao >N-0.2 ) || (orientacao <S+0.2 && orientacao >S-0.2) ){
			double a = aux-yPos;
			if(fabs(a)<0.3)
				return 1;

		}else{
			double a = aux-xPos;
			if(fabs(a)<0.3)
				return 1;
		}
	return 0;

}