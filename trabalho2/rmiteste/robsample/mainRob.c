
/* mainRob.C
 *
 * Basic Robot Agent
 * Very simple version for demonstration
 *
 * For more information about the CiberRato Robot Simulator 
 * please see http://microrato.ua.pt/ or contact us.
 */

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "RobSock.h"

#include "robfunc.h"

#define true 1
#define false 0
double error = 0;
double u = 0;
double error_ant, errorA;
double lu = 0;
double lerror = 0;
double lerror_ant;

double Right[7] = {0, 0, 0, 0, 0, 0, 0};
double Center[7] = {0, 0, 0, 0, 0, 0, 0};
double Left[7] = {0, 0, 0, 0, 0, 0, 0};
int map[28][28];
int dist_x, dist_y;

//int xA,yA;
int xAA = 0, yAA = 0;

int checkWall(void) {
    int aux;
    int x = (int) round(xPos / 2) + 14, y = -(int) round(yPos / 2) + 14;
    aux = MAPA[-(int) round(yPos / 2) + 14][(int) round(xPos / 2) + 14].wall;
    return aux;

}

int DetermineMovement(void) {
    int aux;
    int x = (int) round(xPos / 2) + 14, y = -(int) round(yPos / 2) + 14;
    aux = MAPA[-(int) round(yPos / 2) + 14][(int) round(xPos / 2) + 14].wall;
    //ver celulas adjacentes
    int i;
    for (i = 255; i > 0; i--) {

        if ((((aux & NORTH) != NORTH)&& (MAPA[y - 1][x].pos == i)))
            return 1;
        if ((((aux & SOUTH) != SOUTH) && (MAPA[y + 1][x].pos == i)))
            return 2;
        if ((((aux & EAST) != EAST) && (MAPA[y][x + 1].pos == i)))
            return 4;
        if ((((aux & WEST) != WEST) && (MAPA[y][x - 1].pos == i)))
            return 8;


    }
    return 0;

}

void midMov(float *lPow, float *rPow, int *yA, int *xA,int posA){
        int posB=posA;
        int x=*xA,y=*yA;
       
        int sair=0;
        float a = fabs(round(yPos))-fabs(yPos);
	
	float b = fabs(round(xPos))-fabs(xPos);
        float orientacao =orierror();
        if ((orientacao <N+0.2 && orientacao >N-0.2 ) || (orientacao <S+0.2 && orientacao >S-0.2) ){
        	while( sair==0){
        		
        		ReadSensors();
            	DetermineAction(0, lPow, rPow);
            	actualizaXY(&y, &x);
            	moveForward(lPow, rPow);
            	DriveMotors(*lPow, *rPow);
            	a = fabs(round(yPos))-fabs(yPos);
				if ((int)fabs(fmod(round(yPos),2))==0 && fabs(a) <0.15){
					sair=1;
				}
            	
        	}
        }else{
        	while( sair==0){
        		ReadSensors();
            	DetermineAction(0, lPow, rPow);
            	actualizaXY(&y, &x);
            	
            	moveForward(lPow, rPow);
            	DriveMotors(*lPow, *rPow);
            	b = fabs(round(xPos))-fabs(xPos);
            	if ((int)fabs(fmod(round(xPos),2))==0 && fabs(b) <0.05){
					sair=1;
				}
        	}
				
        }
            	*yA=y;
        		*xA=x;


}

void gotoMiddleCell(float *lPow, float *rPow, int *yA, int *xA) {
    float orientacao = orierror();
    int x_aux = *xA, y_aux = *yA;
    if ((orientacao <N+0.2 && orientacao >N-0.2 ) || (orientacao <S+0.2 && orientacao >S-0.2) ){
        while (y_aux == *yA) {
            ReadSensors();
            DetermineAction(0, lPow, rPow);
            moveForward(lPow, rPow);
            DriveMotors(*lPow, *rPow);
            mapAct(yA, xA);
        }
    } else{
        while (x_aux == *xA) {
            ReadSensors();
            DetermineAction(0, lPow, rPow);
            moveForward(lPow, rPow);
            DriveMotors(*lPow, *rPow);

            mapAct(yA, xA);
        }

    }
}

void rodarX(float *lPow, float *rPow, float anglo) {
    fprintf(stderr, "rodarX lp %f rp %f a %f\n", *lPow, *rPow, anglo);
    float err = teta;
    float errA;
    int sair = 0;
    while (sair == 0) {
    	ReadSensors();
        DetermineAction(0,&lPow,&rPow);
        sair = rotateRel(lPow, rPow, 0.03, &anglo);
        DriveMotors(*lPow, *rPow);
        //errA=err;
        //err=teta;
        //anglo=anglo-(err-errA);
        printf("\n sair %d \n auxangle %f \n", sair, anglo);
        //	auxangle=rotate(&lPow,&rPow,auxangle,0);	//rodar
        //cleanSensors();
    }
}

void needrotation(float *lPow, float *rPow, int dir) {
    fprintf(stderr, "needR lp %f rp %f d %d\n", *lPow, *rPow, dir);
    float auxh = 0;
    if (dir == NORTH) {
        auxh = PI / 2; //-auxh;
        rodarX(lPow, rPow, auxh);
    } else if (dir == SOUTH) {
        auxh = -PI / 2; //-auxh;
        rodarX(lPow, rPow, auxh);
    } else if (dir == EAST) {
        auxh = 0; //-auxh;
        rodarX(lPow, rPow, auxh);
    } else if (dir == WEST) {
        auxh = PI; //-auxh;
        rodarX(lPow, rPow, auxh);
    }
}

int main(int argc, char *argv[]) {
    char host[100] = "localhost";
    char rob_name[20] = "robsample";
    float lPow, rPow;
    int xA, yA;
    int state = STOP, stoppedState = RUN, rob_id = 1;
    int beaconToFollow = 0;

    printf(" Sample Robot\n Copyright (C) 2001-2016 Universidade de Aveiro\n");

    /* processing arguments */
    while (argc > 2) /* every option has a value, thus argc must be 1, 3, 5, ... */ {
        if (strcmp(argv[1], "-host") == 0) {
            strncpy(host, argv[2], 99);
            host[99] = '\0';
        } else if (strcmp(argv[1], "-robname") == 0) {
            strncpy(rob_name, argv[2], 19);
            rob_name[19] = '\0';
        } else if (strcmp(argv[1], "-pos") == 0) {
            if (sscanf(argv[2], "%d", &rob_id) != 1)
                argc = 0; /* error message will be printed */
        } else {
            break; /* the while */
        }
        argc -= 2;
        argv += 2;
    }

    if (argc != 1) {
        fprintf(stderr, "Bad number of parameters\n"
                "SYNOPSIS: mainRob [-host hostname] [-robname robotname] [-pos posnumber]\n");

        return 1;
    }

    /* Connect Robot to simulator */
    if (InitRobot(rob_name, rob_id, host) == -1) {
        printf("%s Failed to connect\n", rob_name);
        exit(1);
    }
    printf("%s Connected\n", rob_name);
    state = STOP;
    initMap();
    //int x=(int)round(yPos/2)+14,y=-(int)round(yPos/2)+14,pos=MAPA[y][x].pos,posant=0;
    int meio = 0;
    yA = 14, xA = 14;
    //	randomChoose(&x,&y);
    int algo=0;
    int firstRun=0;
    while (1) {
        /* Reading next values from Sensors */

        ReadSensors();
        DetermineAction(0, &lPow, &rPow);
        cleanSensors();
        mapAct(&yA, &xA); //actualizar o mapa
        if (GetFinished()) /* Simulator has received Finish() or Robot Removed */ {
            printf("%s Exiting\n", rob_name);
            exit(0);
        }
        if (state == STOP && GetStartButton()) state = stoppedState; /* Restart     */
        if (state != STOP && GetStopButton()) {
            stoppedState = state;
            state = STOP; /* Interrupt */
        }


        

        switch (state) {
            case RUN: /* Go */
            		
                if (GetVisitingLed()) state = WAIT;
                if (GetGroundSensor() == 0) { /* Visit Target */
                    SetVisitingLed(true);
                } else {

                    if(firstRun==0){
            			rodarX(&lPow, &rPow, N);
            			if(analogSensors.array[2] > 0.7)
            				algo= algo | NORTH;
            			firstRun=1;
            		}if(firstRun==1){
            			rodarX(&lPow, &rPow, W);
            			if(analogSensors.array[2] > 0.7)
            				algo= algo | WEST;
            			firstRun=2;
            		}if(firstRun==2){
            			rodarX(&lPow, &rPow, S);
            			if(analogSensors.array[2] > 0.7)
            				algo= algo | SOUTH;
            			firstRun=3;
            		}if(firstRun==3){
            			rodarX(&lPow, &rPow, E);
            			if(analogSensors.array[2] > 0.7)
            				algo= algo | EAST;
            			firstRun=4;
            		}if(firstRun==4){
            			MAPA[yA][xA].wall=algo;
            			firstRun++;
            		}
            	printf("firstRun %u\n",firstRun);
                    int ori = orierror();
                    int checkwall = checkWall();
                    int wal=MAPA[yA][xA].wall;

                    if ((analogSensors.array[2] > 3)) {	//o valor 4 resulta bem
                        int dir = DetermineMovement();
                        needrotation(&lPow, &rPow, dir);
                  
                    } else {
                    	//-------------------
						if( (ori <N+0.2 && ori >N-0.2 )){
							if (MAPA[yA-1][xA].pos< 250){
								int dir = DetermineMovement();
								needrotation(&lPow, &rPow, dir);					
							}else{
							moveForward(&lPow, &rPow);
                        	DriveMotors(lPow, rPow);
                        	}
						}else if( (ori S<+0.2 && ori >S-0.2 )){
							if (MAPA[yA+1][xA].pos< 250){
								int dir = DetermineMovement();
								needrotation(&lPow, &rPow, dir);					
							}else{
							moveForward(&lPow, &rPow);
                        	DriveMotors(lPow, rPow);
                        	}
						}else if( (ori <E+0.2 && ori >E-0.2 )){
							if (MAPA[yA][xA+1].pos< 250){
								int dir = DetermineMovement();
								needrotation(&lPow, &rPow, dir);					
							}else{
							moveForward(&lPow, &rPow);
                        	DriveMotors(lPow, rPow);
                        	}
						}else if( (ori <W+0.2 && ori >W-0.2 )){
							if (MAPA[yA][xA-1].pos< 250){
								int dir = DetermineMovement();
								needrotation(&lPow, &rPow, dir);					
							}else{
							moveForward(&lPow, &rPow);
                        	DriveMotors(lPow, rPow);
                        	}
						}else{
							moveForward(&lPow, &rPow);
                        	DriveMotors(lPow, rPow);}
                    	//------------------
                        //moveForward(&lPow, &rPow);
                        //DriveMotors(lPow, rPow);
                    }
                }
                if (chao == 0) {
                    SetReturningLed(true);
                    state = RETURN;
                }
                break;
            case WAIT: /* Wait for others to visit target */
                SetReturningLed(true);
                if (GetVisitingLed()) SetVisitingLed(true);
                if (GetReturningLed()) state = RETURN;
                DriveMotors(0.0, 0.0);
                break;

            case RETURN: /* Return to home area */
                if (GetVisitingLed()) SetVisitingLed(true);
                SetReturningLed(true);
                int ori = orierror();
                if(meio==0){
                	gotoMiddleCell(&lPow, &rPow, &yA, &xA);
                //	needrotation(&lPow, &rPow, findCloser(&yA,&xA));
                	meio=1;
                
                }else{
                // Wander
                		if (fabs(yPos) < 0.5 && fabs(xPos) < 0.5) {
                    		DriveMotors(0.0, 0.0);
                		} else {
                    		//Astar Agent implementation...
                    		if(MAPA[yA][xA].pos==0){
                    			DriveMotors(0.0,0.0);
                    		}else{
                            needrotation(&lPow, &rPow, findCloser(&yA, &xA));
                            int posB=MAPA[yA][xA].pos;
                            	while(posB==MAPA[yA][xA].pos){
                            		ReadSensors();
      								DetermineAction(0, &lPow, &rPow);
                          			moveForward(&lPow, &rPow);
                     				DriveMotors(lPow, rPow);
        							actualizaXY(&yA,&xA);
        							printf("sighispdfhv%lu\n",MAPA[yA][xA].pos);
                            	}
                            	midMov(&lPow, &rPow, &yA, &xA,MAPA[yA][xA].pos);
                    		}//else
                    			//needrotation(&lPow, &rPow, findCloser(&yA, &xA));
                      		DriveMotors(lPow, rPow);
                    //}
                	}
                printf("ola->return NOW!!\n");
				}
                break;
        }
        printf("x-> %u , y-> %u \n", xA, yA);
        printf("posicao mapa %u \n", MAPA[yA][xA].pos);
     
    }
    printf("vou sair");
    return 1;
}

void randomChoose(int *x, int *y) {
    *x = rand() % 24;
    *y = rand() % 24;
}