#include "rmi-mr32.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "movimento.h"


double x,y,t;


int main(void)
{
	int lapCount=0;		//conta voltas
	int crus=0;		//conta os crusamentos podera nao ser usado
	initPIC32();
	closedLoopControl( true );
	setVel2(0, 0);
	int aux;
	//   printf("RMI-example, robot %d\n\n\n", ROBOT);

	while(1)
	{
		printf("Press start to continue\n");
		while(!startButton());
		enableObstSens();
		do{
			delay(50);		//wait de 50ms
			readAnalogSensors();
			setVel2(30,30);		//implica que teremos de por o robo um pouco atras da linha para o calibrar de inicio
			printf("ola %d \n" ,groundSensor);
		}while(readGround()<16);

		setVel2(0,0);
		setRobotPos(0,0,0);
		getRobotPos(&x,&y,&t);
		waitTick20ms();
		//andar ate se fazer duas voltas	
		do
		{
			waitTick40ms();						// Wait for next 40ms tick
			moveForward(aux=readGround());
			if(aux<16)
				lapCount++;

		} while(lapCount<2);
		waitTick40ms();
		rotateRel(100, -M_PI);	//rodar 180º e fazer a ultima volta
		setVel2(0,0);
		do{

		}while (lapCount!=3);

		disableObstSens();
	}
	return 0;
}

