#include "rmi-mr32.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "movimento.h"
#include "bibaux.h"
#define ESPECIAL 20
#define CRUSMETA 0
double x,y,t;

int flag=1;	//flag para caminho a preto ou branco 0 -> preto 1-> branco
int anguloDesviado [32]= {0,6,2,4,0,ESPECIAL,1,ESPECIAL,-2,ESPECIAL,ESPECIAL,-2,-1,ESPECIAL,0,ESPECIAL,-6,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,-4,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL}; //tabela com a difrenca de angulos e corrigir

double u=0;
double erro_actual=0;
double erro_anterior=0;


int main(void) {
	initPIC32();
	closedLoopControl(true);
	setVel2(0,0);
	printf("Press Start to continue\n");
	while(!startButton());	
	enableObstSens();
	//apos calibracao parar o robo e por os seus estados a zero
	setVel2(0,0);
	setRobotPos(0,0,0);	//fazer reset a posicao
	getRobotPos(&x,&y,&t); //actualizar as variaveis
	//inicialisacao do programa principal
	int angle=4; //variavel auxiliar para saber o valor do angulo
	double u=0;		//sinal de controlo
	int velocidadeBase=60;	//velocidade base do robo
	int valor=0;		//variavel que contem o valor do anguloDesviado	
	int countV=1;		//conta voltas
	int countA=0;
	for(u=0;u<6;u++){	//apenas serve para descartar as primeiras leituras
		waitTick40ms();
		readAnalogSensors();
	}
	while(1){
		do{
			waitTick10ms();
			if(countA%2==0)
				readAnalogSensors();
			countA++;
			//	readAnalogSensors();
			if(!(obstacleSensor(OBST_SENSOR_FRONT)<17)){	//nao existe obstaculo   este dav 15
				led(1,0);
				angle=readLineCond(40);	//0 implica ganho de 50 se tiver erros de leitura, devido a luz alterar o valor
				valor=anguloDesviado[angle];
				if(valor==ESPECIAL){		//casos especiais como curvas apertadas
					countV=specialCases(valor,angle,countV);	
				}else{
					normalNavigation(velocidadeBase,angle);
				}
			}else{
				setVel2(0,0);
				int vel=7;
				led(1,1);
				while( ((obstacleSensor(OBST_SENSOR_FRONT)>=8 ) &&  (obstacleSensor(OBST_SENSOR_FRONT)<20) ) ){
					waitTick40ms();
					readAnalogSensors();
					angle=readLineCond(30);
					normalNavigation(vel,angle);
				}
				if(obstacleSensor(OBST_SENSOR_FRONT)<12){
					setVel2(0,0);
					readAnalogSensors();
					getRobotPos(&x,&y,&t);
					contornarObestaculo(y,x,0);
				}
			}
		}while(!stopButton());
		countV=0;
	}
	return 0;
}

