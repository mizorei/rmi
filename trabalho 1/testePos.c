#include "rmi-mr32.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "movimento.h"
#include "bibaux.h"
int flag=0;
double x,y,t;
int main(void) {
	initPIC32();
	closedLoopControl(true);
	setVel2(0,0);
	printf("Press Start to continue\n");
	//fazer uma inicialisacao do robo e uma calibracao com a linha de partida... podera ser removido eventualmente
	//--------------------------------
	while(!startButton());	
	enableObstSens();
	setVel2(0,0);
	setRobotPos(0,0,0);	//fazer reset a posicao
	getRobotPos(&x,&y,&t); //actualizar as variaveis

	while(1) {
		wait(3);
		emFrente(260);
		setVel2(0,0);
		printf("%lf-x \n %lf -y \n", x,y);
	}
	return 0;
}
