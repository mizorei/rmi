#include "rmi-mr32.h"
#include <math.h>
#include "movimento.h"
#include "bibaux.h"

//ganhos da rotacao do prof
#define KP_ROT	40
#define KI_ROT	5
//#define KP_ROT	30
//#define KI_ROT	3


//ganho do meu PD_linha
#define GANHODIF 3
#define GANHO 8.5


// deltaAngle in radians
extern int anguloDesviado [32];//= {0,6,2,4,0,ESPECIAL,1,ESPECIAL,-2,ESPECIAL,ESPECIAL,-2,-1,ESPECIAL,0,ESPECIAL,-6,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,-4,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL,ESPECIAL}; //tabela com a difrenca de angulos e corrigir


extern double u,erro_actual,erro_anterior;
extern double x,y,t;
//static int aux2=0;
void rotateRel(int maxVel, double deltaAngle)
{
	//double x, y, t;
	double targetAngle;
	double error;
	double integral = 0;
	int cmdVel;

	getRobotPos(&x, &y, &t);
	targetAngle = normalizeAngle(t + deltaAngle);
	do
	{
		waitTick40ms();
		getRobotPos(&x, &y, &t);
		error = normalizeAngle(targetAngle - t);

		integral += error;
		integral = integral > PI / 2 ? PI / 2: integral;
		integral = integral < -PI / 2 ? -PI / 2: integral;

		cmdVel = (int)((KP_ROT * error) + (integral * KI_ROT));
		cmdVel = cmdVel > maxVel ? maxVel : cmdVel;
		cmdVel = cmdVel < -maxVel ? -maxVel : cmdVel;

		setVel2(-cmdVel, cmdVel);
	} while (fabs(error) > 0.01);
	setVel2(0, 0);
}


void rotateRel_basic(int speed, double deltaAngle)
{
	//double x, y, t;
	double targetAngle;
	double error;
	int cmdVel, errorSignOri;

	getRobotPos(&x, &y, &t);
	targetAngle = normalizeAngle(t + deltaAngle);
	error = normalizeAngle(targetAngle - t);
	errorSignOri = error < 0 ? -1 : 1;

	cmdVel = error < 0 ? -speed : speed;
	setVel2(-cmdVel, cmdVel);

	do
	{
		getRobotPos(&x, &y, &t);
		error = normalizeAngle(targetAngle - t);
	} while (fabs(error) > 0.01 && errorSignOri * error > 0);
	setVel2(0, 0);
}
void rotateTillLine(int dir){
	emFrenteV(90,50);
	int angle=0;
	int roda=50;
	if (dir==1)
		roda=-roda;
	while( !( angle==4 || angle ==6 || angle ==8 || angle ==10 || angle ==12 || angle ==14 )){
		//	readAnalogSensors();
		setVel2(roda,-roda);
		angle=readLineCond(30); //27
	}


}

void normalNavigation(int v,int angle){
	erro_anterior=erro_actual;
	erro_actual=anguloDesviado[angle];
	u = GANHO*erro_actual + GANHODIF*(erro_actual-erro_anterior);
	setVel2(v+u,v-u);


}
