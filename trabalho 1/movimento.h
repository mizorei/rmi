#ifndef MOVIMENTO_H    /* Guard against multiple inclusion */

#define MOVIMENTO_H

void rotateRel_basic(int speed, double deltaAngle); 	//funcao retirada do codigo exemplo fornecida na primeira aula
void rotateRel(int maxVel, double deltaAngle);		//funcao retirada do codigo exemplo fornecida na primeira aula
void moveForward(int linha);	//funcao nao utilizada
void rotateTillLine(int dir);	//roda no sentido dos ponteiros do relocio ou contra ate encontrar linha
void normalNavigation(int v,int angle);
#define MIDDLE 4
#define MIDDLE_LEFT 12 
#define MIDDLE_RIGHT 6
#define RIGHT_MIDDLE 2
#define LEFT_MIDDLE 8
#define RIGHT 1
#define LEFT  16
#define FULL 31
#define EMPTY 0

#endif 
