#ifndef BIBAUX_H    /* Guard against multiple inclusion */

#define BIBAUX_H
int meta (int count);
int readLineCond(int);
void contornarObestaculo(double yaux, double xaux,int voltas);
void emFrente(double andar);
void emFrenteV(double andar,int velocidade);
int specialCases(int valor,int angle,int countV);
#endif 
