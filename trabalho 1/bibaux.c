#include "rmi-mr32.h"
#include "movimento.h"
#include "bibaux.h"
#include <math.h>
extern double x,y,t;

extern int flag;
int readLineCond(int gain){
	int a=readLineSensors(gain);
	if (flag)
		a = ~a & 0x01F;
	return a;

}

int checkLine(int xaux,int yaux){
	getRobotPos(&x,&y,&t);

	if( (x<(xaux+200) && x> (xaux-200)) )
		return 0;
	if( (y<(yaux+200) && y> (yaux-200)) )
		return 0;
	return 1;
}

void contornarObestaculo(double yaux,double xaux,int volta){
	if(volta>=3)		//protecao
		return;
	setVel2(0,0);
	readAnalogSensors();
	if (volta==0)
		rotateRel(50,M_PI/2);	//1ºcurva
	else
		rotateRel(50,-M_PI/2); //2º e 3ª curva
	int velocidade=40;
	int angle;
	emFrenteV(40,40);
	waitTick40ms();
	readAnalogSensors();
	waitTick40ms();
	readAnalogSensors();
	waitTick40ms();
	readAnalogSensors();
	setVel2(velocidade,velocidade);
	if(volta<2){					//contarna a vontade dois lados o 3º e precisso de ter mais cuidado
		while(obstacleSensor(OBST_SENSOR_RIGHT)<45){	//contorna o objecto
			waitTick40ms();
			readAnalogSensors();
		} 
		getRobotPos(&x,&y,&t);
		emFrenteV(290,60);
		volta++;
		contornarObestaculo(yaux,xaux,volta);
	}else{						//ir andando para a frente e ter em atencao a leitura da linha do chao
		//emFrenteV(30,25);
		setVel2(25,25);
		//vai andar ate encontrar linha E pela odometria estar dentro dum range admisivel
		while((((angle=readLineCond(40))==0) || checkLine(xaux,yaux))){
			waitTick20ms();
			readAnalogSensors();
		}
		emFrenteV(90,10);
		angle=0;
		setVel2(0,0);
		rotateRel(100,M_PI/2);
	}
}

void emFrente(double dist){
	double errorx,errory;
	double targetx,targety;
	getRobotPos(&x, &y, &t);
	targetx = x+(cos(t)*dist);
	targety = y+(sin(t)*dist);
	setVel2(50,50);
	do{
		readAnalogSensors();
		getRobotPos(&x, &y, &t);
		errorx = targetx-x;
		errory = targety-y; 
	} while ((fabs(errorx) + fabs(errory)) > (dist/7));
}

void emFrenteV(double dist,int Vel){
	double errorx,errory;
	double targetx,targety;
	getRobotPos(&x, &y, &t);
	targetx = x+(cos(t)*dist);
	targety = y+(sin(t)*dist);
	setVel2(Vel,Vel);
	do{
		readAnalogSensors();
		getRobotPos(&x, &y, &t);
		errorx = targetx-x;
		errory = targety-y; 
	} while ((fabs(errorx) + fabs(errory)) > (dist/7));
}


int meta (int count){
	count ++;
	setRobotPos(0,0,0);
	emFrente(90);

	if(count==3){			// rodar180º
	
		rotateRel(100,M_PI);
		setRobotPos(0,0,0);
	}

	if(count==4){			//condicao de paragem
	
		setVel2(0,0);
		while(!stopButton());
	}
	return count;
}

int specialCases(int valor,int angle,int countV){
	getRobotPos(&x,&y,&t);
	if((fabs(x)<120) && (fabs(y) <120) && (angle ==0x1F || angle == 0x0F || angle == 0x1E)){
		countV=meta(countV);
	}else
		switch(angle){
			case 15:
			case 7:			//vira a direita
			case 1:
			case 3:
				rotateTillLine(0);
				break;
			case 30: 
			case 28:
			case 16:
			case 24:
				rotateTillLine(1);
				break;
		}
 return countV;
}
