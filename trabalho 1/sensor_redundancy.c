#include "sensor_redundancy.h"
#include "rmi_mr32.h"

#define HISTORY_SIZE 3

static int history[HISTORY_SIZE] =  {0, 0, 0};
static int pointer = 0;
extern int readLineSensors(int);
void updateGround(void)
{
	int tmp = readLineSensors(0);
	history[pointer] = tmp;

	pointer = ++pointer > HISTORY_SIZE-1 ? 0 : pointer; 
}
int readGround(void)
{	
	if((history[0] + history[1]) / 2 == history[0])
		return history[0];
	return history[2];
}
